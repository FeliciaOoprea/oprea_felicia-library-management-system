package Controller;

import Model.Book;
import Model.Client;
import Model.Loan;
import Repository.BooksRepository;
import Repository.ClientsRepository;
import Repository.LoansRepository;
import View.ClientInterface;

import javax.swing.*;

import static java.lang.Integer.parseInt;

public class ControllerClient extends JFrame {
    private final ClientInterface clientInterface;
    private final ClientsRepository clientsRepository;
    private final BooksRepository booksRepository;
    private final LoansRepository loansRepository;

    public ControllerClient(ClientInterface clientInterface,
                            ClientsRepository clientsRepository,
                            BooksRepository booksRepository,
                            LoansRepository loansRepository) {
        this.clientInterface = clientInterface;
        this.clientsRepository = clientsRepository;
        this.booksRepository = booksRepository;
        this.loansRepository = loansRepository;
    }

    private Client getClient(int id) {
        for (Client client : clientsRepository.getClients()) {
            if (client.getID() == id) return client;
        }
        return null;
    }

    private Book getBook(int id) {
        for (Book book : booksRepository.getBooks()) {
            if (book.getID() == id) return book;
        }
        return null;
    }

    private Loan getLoan(int id,int idClient, int idBook) {
        for (Loan loan : loansRepository.getLoans()) {
            if (loan.getID() == id && loan.getIdBook() == idBook && loan.getIdClient() == idClient) return loan;
        }
        return null;
    }

    private int getIdClient() {
        int k = 0;
        for (Client client : this.clientsRepository.getClients()) {
            if (client.getID() >= k) {
                k = client.getID();
            }
        }
        return k + 1;
    }

    private int getIdLoan() {
        int k = 0;
        for (Loan loan : this.loansRepository.getLoans()) {
            if (loan.getID() >= k) {
                k = loan.getID();
            }
        }
        return k + 1;
    }

    private boolean validForAddClient() {

        if (clientInterface.getNameTextField().getText().isEmpty() ||
                clientInterface.getEmailTextField().getText().isEmpty()) {
            return false;
        }
        return true;

    }

    private boolean validForBorrow() {
        if (clientInterface.getIdClientForBorrowTextField().getText().isEmpty() ||
                clientInterface.getIdBookForBorrowTextField().getText().isEmpty()) {
            return false;
        }
        return true;
    }

    private boolean validForReturn() {
        if (clientInterface.getIdClientForReturnTextField().getText().isEmpty() ||
                clientInterface.getIdBookForReturnTextField().getText().isEmpty()) {
            return false;
        }
        return true;
    }

    private void showBooks() {
        clientInterface.getBookTextArea().setText("");
        String booksToShow = "";
        for (Book book : booksRepository.getBooks()) {
            booksToShow += book.toShow();
        }
        clientInterface.getBookTextArea().setText(booksToShow);
    }

    private void leaveReview(Book book) {
        clientInterface.getLeaveReviewButton().addActionListener(e1 -> {

            if (!clientInterface.getReviewTextField().getText().isEmpty()) {
                try {
                    int review = parseInt(clientInterface.getReviewTextField().getText());
                    if (review >= 0 & review <= 10) {
                        for (Book book1 : booksRepository.getBooks()) {
                            if (book1.getID() == book.getID()) {
                                book.setNrReviews(book.getNrReviews() + 1);
                                book.setNrStars(review + book.getNrStars());
                                booksRepository.updateFile();
                                loansRepository.updateFile();
                                JOptionPane.showMessageDialog(clientInterface,"Thank you for your review! ☺  ");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(clientInterface,"Invalid review. ☹ It must be between 0-10.  ");
                        clientInterface.getReviewTextField().setText("");
                    }

                } catch (RuntimeException er) {
                    JOptionPane.showMessageDialog(clientInterface,"Invalid review. Please try again! ☹");
                    clientInterface.getReviewTextField().setText("");
                }
            } else {
                JOptionPane.showMessageDialog(clientInterface,"Please introduce a review! ☺ ");
                clientInterface.getReviewTextField().setText("");
            }
        });
    }


    public void runClientController() {
        clientInterface.getAddClientButton().addActionListener(e -> {
            clientInterface.getNewClientMessageLabel().setText("");
            if (validForAddClient()) {
                Client client = new Client(this.getIdClient(), clientInterface.getNameTextField().getText(), clientInterface.getEmailTextField().getText());
                try {
                    this.clientsRepository.add(client);
                    clientInterface.getNewClientMessageLabel().setText("Client added successfully! ☺ Your ID is: " + client.getID());
                    this.clientsRepository.updateFile();
                } catch (RuntimeException er) {
                    clientInterface.getNewClientMessageLabel().setText(er.getMessage());
                }
            } else {
                clientInterface.getNewClientMessageLabel().setText("Invalid name or email. ☹ ");
            }

        });

        clientInterface.getViewBooks().addActionListener(e -> {
            showBooks();
        });

        clientInterface.getBorrowButton().addActionListener(e -> {
            clientInterface.getBorrowMessageLabel().setText("");
            if (validForBorrow()) {
                try {
                    int idClient = parseInt(clientInterface.getIdClientForBorrowTextField().getText());
                    int idBook = Integer.parseInt(clientInterface.getIdBookForBorrowTextField().getText());
                    Client client = getClient(idClient);
                    Book book = getBook(idBook);
                    if (book.getNrOfBooks() >= 1) {
                        int idLoan = getIdLoan();
                        Loan loan = new Loan(idLoan, idClient, idBook);
                        loansRepository.add(loan);
                        book.setNrOfBooks(book.getNrOfBooks() - 1);
                        clientInterface.getBorrowMessageLabel().setText("Loan successfully completed.☺ The ID for this loan is:" + idLoan +". Enjoy your reading! ");
                        loansRepository.updateFile();
                        booksRepository.updateFile();
                        showBooks();
                    } else {
                        clientInterface.getBorrowMessageLabel().setText("This book is no longer in stock. Try it later! ☺");
                    }

                } catch (RuntimeException er) {
                    clientInterface.getBorrowMessageLabel().setText("The loan failed. ☹ Please try again! ");
                }

            } else {
                clientInterface.getBorrowMessageLabel().setText("Invalid Id-Client or Id-Book ☹");
            }

        });


        clientInterface.getReturnButton().addActionListener(e -> {
            clientInterface.getReturnMessageLabel().setText("");
            int idClient;
            int idBook;
            int id;
            if (validForReturn()) {
                try {
                    id = parseInt(clientInterface.getIdLoanForReturnTextField().getText());
                    idClient = parseInt(clientInterface.getIdClientForReturnTextField().getText());
                    idBook = parseInt(clientInterface.getIdBookForReturnTextField().getText());
                    Loan loan = getLoan(id,idClient, idBook);
                    Book book = getBook(idBook);
                    assert loan != null;
                    if (!loan.isReturned()) {
                        loan.setReturned(true);
                        assert book != null;
                        book.setNrOfBooks(book.getNrOfBooks() + 1);
                        leaveReview(book);
                        clientsRepository.updateFile();
                        showBooks();
                        clientInterface.getReturnMessageLabel().setText("You have successfully returned the book!☺ We hope you enjoyed it!");
                    } else {
                        clientInterface.getReturnMessageLabel().setText("This book is already returned! ☺");
                    }
                } catch (RuntimeException er) {
                    clientInterface.getReturnMessageLabel().setText("Invalid ID-Client or ID-Book. ☹");

                }
            } else {
                clientInterface.getReturnMessageLabel().setText("Please introduce Id-Client and ID-Book. ☺");

            }

        });


    }
}
