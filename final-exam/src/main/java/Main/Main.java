package Main;

import Controller.ControllerClient;
import Controller.ControllerLibrarian;
import Controller.ControllerLibrary;
import Repository.BooksRepository;
import Repository.ClientsRepository;
import Repository.LoansRepository;
import View.ClientInterface;
import View.LibrarianInterface;
import View.LibraryInterface;

public class Main {
    public static void main(String[] args) {
        LibrarianInterface librarianInterface = new LibrarianInterface();
        ClientInterface clientInterface = new ClientInterface();
        ClientsRepository clientsRepository = new ClientsRepository();
        BooksRepository booksRepository = new BooksRepository();
        LoansRepository loansRepository = new LoansRepository();
        ControllerClient controllerClient = new ControllerClient(clientInterface,
                clientsRepository,booksRepository,loansRepository);
        ControllerLibrarian controllerLibrarian = new ControllerLibrarian(librarianInterface,
                clientsRepository,booksRepository,loansRepository);
        LibraryInterface libraryInterface = new LibraryInterface();
        ControllerLibrary controllerLibrary = new ControllerLibrary(libraryInterface,
                controllerClient, clientInterface,librarianInterface, controllerLibrarian);
        controllerLibrary.runControllerLibrary();
    }
}
