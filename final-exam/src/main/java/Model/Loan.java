package Model;

import java.util.Objects;

public class Loan {
    private int ID;
    private int idClient;
    private int idBook;
    private boolean returned;

    public Loan(int ID, int idClient, int idBook) {
        this.ID = ID;
        this.idClient = idClient;
        this.idBook = idBook;
        this.returned = false;
    }

    public Loan(String s) {
        try {
            String[] loan_split = s.split("/");
            int id = Integer.parseInt(loan_split[0]);
            int idClient = Integer.parseInt(loan_split[1]);
            int idBook = Integer.parseInt(loan_split[2]);
            boolean returned = Boolean.parseBoolean(loan_split[3]);
            this.ID = id;
            this.idClient = idClient;
            this.idBook = idBook;
            this.returned = returned;
        }
        catch (RuntimeException e ) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return idClient == loan.idClient && idBook == loan.idBook && returned == loan.returned;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idClient, idBook, returned);
    }

    public boolean isReturned() {
        return returned;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public int getID() {
        return ID;
    }

    public int getIdClient() {
        return idClient;
    }

    public int getIdBook() {
        return idBook;
    }



    @Override
    public String toString() {
        return  ID + "/" +
                idClient + "/" +
                idBook + "/" +
                returned ;
    }

    public String toShow() {
      return String.format("|%-10d|%-60d|%-50d|%-5b\n", getID(),getIdClient(),getIdBook(),isReturned());
    }
}
