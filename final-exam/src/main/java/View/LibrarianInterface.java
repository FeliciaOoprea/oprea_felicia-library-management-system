package View;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class LibrarianInterface extends JFrame {

    private JButton addBook;
    private JTextField titleTextField;
    private JTextField authorTextField;
    private JTextField nrOfBooksTextField;
    private JLabel addMessageLabel;

    private JButton deleteBook;
    private JTextField idTextField;
    private JLabel deleteMessageLabel;

    private JButton viewClients;
    private JButton hideClients;
    private JTextArea clientTextArea;

    private JButton viewBorrowedBooks;
    private JButton hideBorrowedBooks;
    private JTextArea borrowedBooksTextArea;

    public LibrarianInterface(){
    }

    public void runLibrarianInterface(){
        setTitle("Librarian Home Page");
        setVisible(true);
        setSize(700,1000);
        setLocation(500,10);
        setResizable(false);
        this.repaint();
        this.setLayout(null);
        Border raisedBevelBorder = BorderFactory.createRaisedBevelBorder();
        Border loweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        Font myFont  = new Font("myFont",Font.PLAIN,20);

        JLabel welcomeLabel = new JLabel();
        welcomeLabel.setBounds(50,20,590,40);
        welcomeLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(welcomeLabel);
        JLabel welcomeMessageLabel = new JLabel("\uD83D\uDCD6 \uD83D\uDCD6    Welcome back!    \uD83D\uDCD6 \uD83D\uDCD6");
        welcomeMessageLabel.setBounds(200,30,400,30);
        welcomeMessageLabel.setFont(myFont);
        add(welcomeMessageLabel);

        JLabel addLabel = new JLabel("");
        addLabel.setBounds(20,70,650,210);
        addLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(addLabel);
        JLabel titleLabel = new JLabel("Title:");
        titleLabel.setBounds(160,100,70,20);
        titleLabel.setFont(myFont);
        add(titleLabel);
        titleTextField = new JTextField();
        titleTextField.setBounds(220,90,350,30);
        titleTextField.setFont(myFont);
        add(titleTextField);
        JLabel authorLabel = new JLabel("Author:");
        authorLabel.setBounds(140,140,70,20);
        authorLabel.setFont(myFont);
        add(authorLabel);
        authorTextField = new JTextField();
        authorTextField.setBounds(220,130,350,30);
        authorTextField.setFont(myFont);
        add(authorTextField);
        JLabel nrOfBooksLabel = new JLabel("Quantity:");
        nrOfBooksLabel.setBounds(120,180,100,20);
        nrOfBooksLabel.setFont(myFont);
        add(nrOfBooksLabel);
        nrOfBooksTextField = new JTextField();
        nrOfBooksTextField.setBounds(220,170,350,30);
        nrOfBooksTextField.setFont(myFont);
        add(nrOfBooksTextField);
        addBook = new JButton("Add new book");
        addBook.setBounds(240,210,200,30);
        addBook.setFont(myFont);
        add(addBook);
        addMessageLabel = new JLabel("");
        addMessageLabel.setBounds(40,240,550,30);
        addMessageLabel.setFont(myFont);
        add(addMessageLabel);

        JLabel deleteLabel = new JLabel("");
        deleteLabel.setBounds(20,290,650,120);
        deleteLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(deleteLabel);
        JLabel idLabel = new JLabel("Id of the book:");
        idLabel.setBounds(70,310,200,20);
        idLabel.setFont(myFont);
        add(idLabel);
        idTextField = new JTextField();
        idTextField.setBounds(220,300,350,30);
        idTextField.setFont(myFont);
        add(idTextField);
        deleteBook = new JButton("Delete book from library");
        deleteBook.setBounds(190,340,300,30);
        deleteBook.setFont(myFont);
        add(deleteBook);
        deleteMessageLabel = new JLabel("");
        deleteMessageLabel.setBounds(40,380,500,30);
        deleteMessageLabel.setFont(myFont);
        add(deleteMessageLabel);


        JLabel clientsLabel = new JLabel("");
        clientsLabel.setBounds(20,420,650,240);
        clientsLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(clientsLabel);
        clientTextArea = new JTextArea();
        clientTextArea.setBounds(40,460,610,150);
        clientTextArea.setFont(new Font("Font",Font.PLAIN,15));
        add(clientTextArea);
        JLabel formatClientLabel = new JLabel("ID          Name                                                 Email");
        formatClientLabel.setBounds(50,440,610,20);
        formatClientLabel.setFont(myFont);
        add(formatClientLabel);
        viewClients = new JButton("View clients list");
        viewClients.setBounds(70,620,250,30);
        viewClients.setFont(myFont);
        add(viewClients);
        hideClients = new JButton("Hide clients list");
        hideClients.setBounds(380,620,250,30);
        hideClients.setFont(myFont);
        add(hideClients);

        JLabel booksLabel = new JLabel("");
        booksLabel.setBounds(20,680,650,250);
        booksLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(booksLabel);
        borrowedBooksTextArea = new JTextArea();
        borrowedBooksTextArea.setBounds(40,720,610,150);
        borrowedBooksTextArea.setFont(new Font("Font",Font.PLAIN,15));
        add(borrowedBooksTextArea);
        JLabel formatBookLabel = new JLabel("ID       ID-Client                           ID-Book                       Returned");
        formatBookLabel.setBounds(40,700,610,20);
        formatBookLabel.setFont(myFont);
        add(formatBookLabel);
        viewBorrowedBooks = new JButton("View borrowed books");
        viewBorrowedBooks.setBounds(70,890,250,30);
        viewBorrowedBooks.setFont(myFont);
        add(viewBorrowedBooks);
        hideBorrowedBooks = new JButton("Hide borrowed books");
        hideBorrowedBooks.setBounds(380,890,250,30);
        hideBorrowedBooks.setFont(myFont);
        add(hideBorrowedBooks);


    }

    public JTextField getAuthorTextField() {
        return authorTextField;
    }

    public JTextField getTitleTextField() {
        return titleTextField;
    }


    public JButton getAddBook() {
        return addBook;
    }

    public JTextField getNrOfBooksTextField() {
        return nrOfBooksTextField;
    }

    public JButton getDeleteBook() {
        return deleteBook;
    }

    public JTextField getIdTextField() {
        return idTextField;
    }

    public JButton getViewClients() {
        return viewClients;
    }

    public JButton getHideClients() {
        return hideClients;
    }

    public JButton getViewBorrowedBooks() {
        return viewBorrowedBooks;
    }

    public JButton getHideBorrowedBooks() {
        return hideBorrowedBooks;
    }

    public JTextArea getClientTextArea() {
        return clientTextArea;
    }

    public JLabel getAddMessageLabel() {
        return addMessageLabel;
    }

    public JLabel getDeleteMessageLabel() {
        return deleteMessageLabel;
    }

    public JTextArea getBorrowedBooksTextArea() {
        return borrowedBooksTextArea;
    }


}
