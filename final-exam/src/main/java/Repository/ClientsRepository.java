package Repository;

import Exceptions.InvalidClientException;
import Model.Client;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class ClientsRepository {
    private final ArrayList<Client> clients;
    private static String fileName;

    public ClientsRepository() {
        fileName = "src/main/resources/Clients.txt";
        this.clients = new ArrayList<>();
        this.readFromFile();
    }

    public ArrayList<Client> getClients() {
        return clients;
    }

    public void saveToFile() {
        try {
            FileWriter fileWriter = new FileWriter(fileName, false);
            for (Client client : this.clients) {
                fileWriter.write(client.toString());
                fileWriter.write("\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(
                    String.format("An error occurred while saving to file: %s", e.getMessage()),
                    e
            );
        }
    }

    public void readFromFile() {
       try {
           List<String> all_lines = Files.readAllLines(Paths.get(fileName));
           for (String line : all_lines) {
               Client client = new Client(line);
               clients.add(client);
           }
       } catch (IOException e ) {
           //ignore missing file
       }
        }

        public void add(Client client){
        if (!verifyIfInFile(client)) {
            clients.add(client);
            saveToFile();

        }
        else throw new InvalidClientException("This client already exists!");
        }

    public void updateFile() {
        this.saveToFile();
    }

        public boolean verifyIfInFile(Client client){
        for (Client client1 : this.clients) {
            if (client1.equals(client)) return true;
        }
        return false;
        }



}

